from django import forms
from models.agent_image import AgentImage


class ImageCropDetailsForm(forms.Form):
    width = forms.IntegerField()
    height = forms.IntegerField()
    crop_x = forms.IntegerField()
    crop_y = forms.IntegerField()
    crop_width = forms.IntegerField()
    crop_height = forms.IntegerField()


class AgentImageForm(forms.ModelForm):
    image = forms.ImageField()

    def clean(self):
        clean_data = super(AgentImageForm, self).clean()
        image = clean_data.get("image")
        if image is not None:
            ext = image.name.split('.')[-1]
            if ext.lower() != "jpg":
                raise forms.ValidationError("Looks like the file you tried to upload was not a .jpg file")
        else:
            raise forms.ValidationError("You did not select a file.")
        return clean_data

    class Meta:
        model = AgentImage
        fields = ['image']
