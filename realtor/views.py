import datetime
import json
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.db.models import Sum, Count, Max
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.http import HttpResponse
from forms import *
from models import *


@login_required
def home(request):
    office_count = Office.objects.all().count()
    agent_count = Agent.objects.all().count()
    transaction_count = Transaction.objects.all().count()
    transaction_volume = Transaction.objects.aggregate(Sum('selling_price'))['selling_price__sum']
    trailing_transactions = Transaction.objects.exclude(date_closed_sale__isnull=True)\
        .filter(date_closed_sale__gt = datetime.datetime.today()- datetime.timedelta(365) )\
        .extra({'month_bin':"to_date(to_char(date_closed_sale,'01-MM-YYYY'),'DD-MM-YYYY')"}).values('month_bin')\
        .annotate(monthly_transaction_volume=Sum('selling_price'), monthly_transaction_count=Count('selling_price'))\
        .order_by('month_bin')
    context = {'office_count':office_count, 'agent_count':agent_count, 'transaction_count':transaction_count,
               'transaction_volume':transaction_volume, 'trailing_transactions':trailing_transactions}
    return render(request, "realtor/home.html", context)


@login_required
def realtor_list(request):
    agents = Agent.objects.select_related('agentimage','office')\
                 .filter(total_selling_transaction_volume__gt=0)\
                 .filter(valid=True)\
                 .order_by('-total_selling_transaction_volume')
    max_selling_volume = Agent.objects.all().aggregate(Max('total_selling_transaction_volume'))['total_selling_transaction_volume__max']
    max_listing_volume = Agent.objects.all().aggregate(Max('total_listing_transaction_volume'))['total_listing_transaction_volume__max']
    paginator = Paginator(agents, 100)
    page = request.GET.get('page', '1')
    try:
        agents = paginator.page(page)
    except (EmptyPage, InvalidPage):
        agents = paginator.page(paginator.num_pages)
    context = {'agents':agents, 'agent_image_model':AgentImage, 'max_selling_volume':max_selling_volume,
               'max_listing_volume':max_listing_volume}
    return render(request, "realtor/realtor_list.html", context)

@login_required
def invalid_realtor_list(request):
    agents = Agent.objects.select_related('agentimage','office')\
                 .filter(valid=False)\
                 .order_by('-total_selling_transaction_volume')
    max_selling_volume = Agent.objects.all().aggregate(Max('total_selling_transaction_volume'))['total_selling_transaction_volume__max']
    max_listing_volume = Agent.objects.all().aggregate(Max('total_listing_transaction_volume'))['total_listing_transaction_volume__max']
    paginator = Paginator(agents, 100)
    page = request.GET.get('page', '1')
    try:
        agents = paginator.page(page)
    except (EmptyPage, InvalidPage):
        agents = paginator.page(paginator.num_pages)
    context = {'agents':agents, 'agent_image_model':AgentImage, 'max_selling_volume':max_selling_volume,
               'max_listing_volume':max_listing_volume}
    return render(request, "realtor/realtor_list.html", context)




@login_required
def office_list(request):
    offices = Office.objects.all().order_by("-total_selling_transaction_volume")

    paginator = Paginator(offices, 100)
    page = request.GET.get('page', '1')
    try:
        offices = paginator.page(page)
    except (EmptyPage, InvalidPage):
        offices = paginator.page(paginator.num_pages)
    context = {'offices':offices,}
    return render(request, "realtor/office_list.html", context)


@login_required
def one_realtor(request, realtor_id):
    agent = get_object_or_404(Agent, pk=realtor_id)
    related_agents = Agent.objects.filter(valid=True).filter(license_number=agent.license_number).exclude(pk=agent.id) if agent.license_number else []
    selling_transactions = Transaction.objects.select_related('listing_agent','address','listing_agent__agentimage').filter(selling_agent_id=realtor_id)
    co_selling_transactions = Transaction.objects.select_related('listing_agent','address','listing_agent__agentimage').filter(co_selling_agent_id=realtor_id)
    listing_transactions = Transaction.objects.select_related('selling_agent','address','selling_agent__agentimage').filter(listing_agent_id=realtor_id)
    co_listing_transactions = Transaction.objects.select_related('selling_agent','address','selling_agent__agentimage').filter(co_listing_agent_id=realtor_id)
    listing_type_transaction_count = listing_transactions.count() + co_listing_transactions.count()
    selling_type_transaction_count =  selling_transactions.count() + co_selling_transactions.count()
    total_transaction_count = listing_type_transaction_count + selling_type_transaction_count
    listing_transactions_by_month = Transaction.objects.exclude(date_closed_sale__isnull=True)\
        .filter(date_closed_sale__gt = datetime.datetime.today()- datetime.timedelta(365) )\
        .filter(listing_agent_id=realtor_id)\
        .extra({'month_bin':"to_date(to_char(date_closed_sale,'01-MM-YYYY'),'DD-MM-YYYY')"})\
        .values('month_bin')\
        .annotate(monthly_transaction_count=Count('selling_price'))\
        .order_by('month_bin')
    selling_transactions_by_month = Transaction.objects.exclude(date_closed_sale__isnull=True)\
        .filter(date_closed_sale__gt = datetime.datetime.today()- datetime.timedelta(365) )\
        .filter(selling_agent_id=realtor_id)\
        .extra({'month_bin':"to_date(to_char(date_closed_sale,'01-MM-YYYY'),'DD-MM-YYYY')"})\
        .values('month_bin')\
        .annotate(monthly_transaction_count=Count('selling_price'))\
        .order_by('month_bin')
    co_listing_transactions_by_month = Transaction.objects.exclude(date_closed_sale__isnull=True)\
        .filter(date_closed_sale__gt = datetime.datetime.today()- datetime.timedelta(365) )\
        .filter(co_listing_agent_id=realtor_id)\
        .extra({'month_bin':"to_date(to_char(date_closed_sale,'01-MM-YYYY'),'DD-MM-YYYY')"})\
        .values('month_bin')\
        .annotate(monthly_transaction_count=Count('selling_price'))\
        .order_by('month_bin')
    co_selling_transactions_by_month = Transaction.objects.exclude(date_closed_sale__isnull=True)\
        .filter(date_closed_sale__gt = datetime.datetime.today()- datetime.timedelta(365) )\
        .filter(co_selling_agent_id=realtor_id)\
        .extra({'month_bin':"to_date(to_char(date_closed_sale,'01-MM-YYYY'),'DD-MM-YYYY')"})\
        .values('month_bin')\
        .annotate(monthly_transaction_count=Count('selling_price'))\
        .order_by('month_bin')


    context = {'agent': agent,
               'related_agents': related_agents,
               'selling_transactions': selling_transactions,
               'co_selling_transactions': co_selling_transactions,
               'listing_transactions': listing_transactions,
               'co_listing_transactions': co_listing_transactions,
               'listing_transactions_by_month' : listing_transactions_by_month,
               'selling_transactions_by_month' : selling_transactions_by_month,
               'co_listing_transactions_by_month' : co_listing_transactions_by_month,
               'co_selling_transactions_by_month' : co_selling_transactions_by_month,
               'listing_type_transaction_count' : listing_type_transaction_count,
               'selling_type_transaction_count' : selling_type_transaction_count,
               'total_transaction_count' : total_transaction_count,
                'agent_image_model':AgentImage}
    return render(request, "realtor/one_realtor.html", context)


@login_required
def edit_image(request, image_id):
    image = get_object_or_404(AgentImage, pk=image_id)
    if request.method == 'POST':
        form = ImageCropDetailsForm(request.POST)
        if form.is_valid():
            image.__dict__.update(form.cleaned_data)
            image.save()
            return redirect('/realtor/'+image_id)

    context = {"image":image}
    return render(request, "realtor/edit_image.html", context)


@login_required
def upload_image(request, realtor_id):
    agent = get_object_or_404(Agent, id=realtor_id)
    if request.method == "POST":
        form = AgentImageForm(request.POST, request.FILES)
        if form.is_valid():
            image = form.save(commit=False)
            image.agent = agent
            image.has_uploaded = True
            image.width = 0
            image.height = 0
            image.crop_x = 0
            image.crop_y = 0
            image.crop_width = 0
            image.crop_height = 0
            image.save()
            agent_image = AgentImage.objects.get(agent=agent)
            image_id = agent_image.pk
            return redirect('/image/'+image_id)
    else:
        form = AgentImageForm()
    context = {"form": form, "agent": agent}
    return render(request, "realtor/upload_image.html", context)


@login_required
def one_office(request, office_id):
    office = get_object_or_404(Office, pk=office_id)
    agents = Agent.objects.filter(office_id=office_id).select_related('agentimage').order_by('-total_selling_transaction_volume')
    max_selling_volume = Agent.objects.all().aggregate(Max('total_selling_transaction_volume'))['total_selling_transaction_volume__max']
    max_listing_volume = Agent.objects.all().aggregate(Max('total_listing_transaction_volume'))['total_listing_transaction_volume__max']
    context={'office':office, 'agents':agents, 'agent_image_model':AgentImage, 'max_selling_volume':max_selling_volume,
               'max_listing_volume':max_listing_volume}
    return render(request, "realtor/one_office.html", context)


@login_required
def one_transaction(request, transaction_id):
    transaction = get_object_or_404(Transaction, pk=transaction_id)
    context = {'transaction': transaction,
               'agent_image_model':AgentImage}
    return render(request, "realtor/one_transaction.html", context)


@login_required
def csv_import_list(request):
    imports = CsvImport.objects.all()
    context={"imports":imports}
    return render(request, "realtor/csv_import_list.html", context)


@login_required
def one_csv_import(request, import_id):
    import_detail = get_object_or_404(CsvImport, pk=import_id)
    context = {'import_detail':import_detail}
    return render(request, "realtor/one_csv_import.html", context)


@login_required
def one_csv_import_json(request, import_id):
    import_detail = get_object_or_404(CsvImport, pk=import_id)
    output = {"progress":import_detail.progress,
                "status":import_detail.status,
                "user_message":import_detail.user_message}

    return HttpResponse(json.dumps(output), content_type="application/json")

@login_required
def admin(request):
    return render(request, "realtor/admin.html")
