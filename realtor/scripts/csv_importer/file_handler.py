
import csv
import traceback
from datetime import datetime


from realtor.models import Address
from realtor.models import Agent
from realtor.models import Office
from realtor.models import Transaction
from realtor.models import CsvImport
from realtor.models import CsvImportLog
from django.db.models import Sum, Count


REQUIRED_FIELD = ['Agent Remarks', 'Area', "Assessor's Parcel Number", 'City',
                   'Co SA  BRE License Number', 'Co-Listing Agent First Name',
                   'Co-Listing Agent Last Name', 'Co-Listing Agent Public ID',
                   'Co-Listing Office Code', 'Co-Listing Office Name',
                   'Co-Listing Office Phone Number', 'Co-Selling Agent First Name',
                   'Co-Selling Agent Last Name', 'Co-Selling Agent Public ID',
                   'Co-Selling Office Code', 'Co-Selling Office Name',
                   'Cumulative Days on Market', 'Current Price', 'Date Closed Sale',
                   'Date Status Changed', 'Days on Market', 'Financing', 'Latitude',
                   'Listing Agent Cell Phone', 'Listing Agent Direct Office Phone',
                   'Listing Agent Email Address', 'Listing Agent First Name',
                   'Listing Agent Last Name', 'Listing Agent Public ID',
                   'Listing Office Code', 'Listing Office Name', 'Listing Office Phone',
                   'Longitude', 'Lot Square Footage', 'Multiple Listing Number',
                   'Price Per Square Foot', 'Property Description', 'SA Dre License Number',
                   'SaleType', 'Selling Agent First Name', 'Selling Agent Last Name',
                   'Selling Agent Public ID', 'Selling Office Code', 'Selling Office Name',
                   'Selling Price', 'Sold Terms', 'Status', 'Street Direction',
                   'Street Direction Suffix', 'Street Name', 'Street Number',
                   'Street Number Modifier', 'Street Suffix', 'Street Suffix Modifier',
                   'Structure Attached?', 'Type', 'Unit Number', 'Year Built', 'Zip Code'
                   ]

# progress bar
progress_background_queued = 1
progress_row_processing_start = 2
progress_row_processing_end = progress_agent_aggregation_start = 70
progress_agent_aggregation_end = progress_office_aggregation_start = 90
progress_office_aggregation_end = 100






#################################################################
######################  HELPER FUNCTIONS    #####################
#################################################################


def issublist(list1, list2):
    """ Returns True if all items of list1 are in list2. Else returns False
    """
    return set(list1).issubset(set(list2))

def items_missing(required, items_available):
    return [i for i in required if i not in items_available]

def normalize_number(a):
    """Convert string to int
    """
    if a.isdigit():
        return a
    else:
        return 0

def int_or_null(a):
    if a.isdigit():
        return a
    else:
        return None


def int_or_zero(a):
    if a:
        return a
    else:
        return 0


def decimal_or_null(a):
    if a:
        return a
    else:
        return None

def str_to_datetime(date):
    """Convert string to datetime
    """
    try:
        return datetime.strptime(date, "%m/%d/%Y %H:%M")
    except:
        return None

def convert_structure_attached_field(value):
    if value.lower() == 'attached':
        return True
    if value.lower() == 'detached':
        return False
    else:
        return None


def get_row_count_in_file(filename):
    file = open(filename,"r")
    num_lines = sum(1 for line in file)
    file.close()
    return num_lines - 1


def calculate_progress(start, end, cur, total, max=100):
    """
    takes 4 integers
    returns int value 0-100
    """
    progress = int( start + ( end - start ) * (1.00 * cur/total) / (max*.01) )
    #print start, end, cur, total
    print progress
    return progress


#################################################################
##################  Create Object Functions    ##################
#################################################################


def add_address(city, street_direction="", street_direction_suffix="", street_name="",
                 street_number=0, street_number_modifier="", street_suffix="",
                 street_suffix_modifier="", unit_number="", zip_code=0,
                 latitude=0, longitude=0):
    """Add Address object.
    """
    try:
        address = Address.objects.get(street_direction = street_direction,
                                         street_direction_suffix = street_direction_suffix,
                                         street_name__iexact = street_name,
                                         street_number = int_or_null(street_number),
                                         street_number_modifier = street_number_modifier,
                                         street_suffix_modifier = street_suffix_modifier,
                                         street_suffix = street_suffix,
                                         unit_number = unit_number,
                                         city = city,
                                         zip_code = int_or_null(zip_code))
        return address
    except Address.DoesNotExist:
        address = Address.objects.create(street_direction = street_direction,
                                         street_direction_suffix = street_direction_suffix,
                                         street_name = street_name,
                                         street_number = int_or_null(street_number),
                                         street_number_modifier = street_number_modifier,
                                         street_suffix_modifier = street_suffix_modifier,
                                         street_suffix = street_suffix,
                                         unit_number = unit_number,
                                         city = city,
                                         zip_code = int_or_null(zip_code),
                                         latitude = latitude,
                                         longitude = longitude)
        address.save()
        return address

def add_agent(id, office_id, email, cell_phone_number="", direct_phone_number="", website="",
              first_name="", last_name="", license_number=0,
              address=None, office_name="", office_phone=""):
    """Create Agent object.
    """
    id = id.upper()
    try:
        agent = Agent.objects.get(id=id)
        if email:
            agent.email = email
        if cell_phone_number:
            agent.cell_phone_number = cell_phone_number
        if direct_phone_number:
            agent.direct_phone_number = direct_phone_number
        if website:
            agent.website = website
        if first_name:
            agent.first_name = first_name
        if last_name:
            agent.last_name = last_name
        if license_number:
            agent.license_number = int_or_null(license_number)
        agent.save()
        return agent
    except Agent.DoesNotExist as e:
        if office_id:
            office_id = office_id.upper()
            try:
                office = Office.objects.get(id=office_id)
            except Office.DoesNotExist:
                office = add_office(office_id, address, office_name, office_phone,
                                    license_number)
                if not office:
                    return None
            agent = Agent.objects.create(id = id,
                                          office = office,
                                          email = email,
                                          cell_phone_number = cell_phone_number,
                                          direct_phone_number = direct_phone_number,
                                          website = website,
                                          first_name = first_name,
                                          last_name = last_name,
                                          license_number = int_or_null(license_number),)
            agent.save()
            return agent
        return None

def add_office(id, address=None, name="", phone="", license_number=0):
    """Create Office object.
    """
    if id:
        office = Office.objects.create(id = id,
                                        address = address,
                                        name = name,
                                        phone = phone,
                                        license_number = int_or_null(license_number))
        office.save()
        return office
    else:
        return None

def add_transaction(multiple_listing_number, status, date_status_changed, address,
                    listing_agent, co_listing_agent=None, selling_agent=None,
                    co_selling_agent=None, selling_price=0, sale_type="", financing="",
                    sold_terms="", assessors_parcel_number=0, structure_attached=None,
                    area="", agent_remarks="", lot_square_footage=0, price_per_square_foot=0,
                    property_description="", year_built=0, property_type="",
                    cumulative_days_on_market=0, current_price=0, date_closed_sale=None,
                    days_on_market=0):
    """Create transaction object.
    """
    try:
        tran = Transaction.objects.get(multiple_listing_number=multiple_listing_number)
        return tran
    except Transaction.DoesNotExist:
        tran = Transaction.objects.create(multiple_listing_number = multiple_listing_number,
                                          status = status,
                                          date_status_changed = str_to_datetime(date_status_changed),
                                          address = address,
                                          listing_agent = listing_agent,
                                          co_listing_agent = co_listing_agent,
                                          selling_agent = selling_agent,
                                          co_selling_agent = co_selling_agent,
                                          selling_price = normalize_number(selling_price),
                                          sale_type = sale_type,
                                          financing = financing,
                                          sold_terms = sold_terms,
                                          assessors_parcel_number = int_or_null(assessors_parcel_number),
                                          structure_attached = convert_structure_attached_field(structure_attached),
                                          area = area,
                                          agent_remarks = agent_remarks,
                                          lot_square_footage = int_or_null(lot_square_footage),
                                          price_per_square_foot = decimal_or_null(price_per_square_foot),
                                          property_description = property_description,
                                          year_built = normalize_number(year_built),
                                          property_type = property_type,
                                          cumulative_days_on_market = normalize_number(cumulative_days_on_market),
                                          current_price = normalize_number(current_price),
                                          date_closed_sale = str_to_datetime(date_closed_sale),
                                          days_on_market = normalize_number(days_on_market))
        tran.save()
        return tran



def import_row(row):

    address = add_address(row["City"], row["Street Direction"],
                           row["Street Direction Suffix"], row["Street Name"],
                           row["Street Number"], row["Street Number Modifier"],
                           row["Street Suffix"], row["Street Suffix Modifier"],
                           row["Unit Number"], row["Zip Code"], row["Latitude"],
                           row["Longitude"])

    listing_agent = add_agent(row["Listing Agent Public ID"],
                               row["Listing Office Code"],
                               row["Listing Agent Email Address"],
                               row["Listing Agent Cell Phone"],
                               row["Listing Agent Direct Office Phone"],
                               "", row["Listing Agent First Name"],
                               row["Listing Agent Last Name"],
                               "", None, row["Listing Office Name"],
                               row["Listing Office Phone"])

    co_listing_agent = add_agent(row["Co-Listing Agent Public ID"],
                                 row["Co-Listing Office Code"],
                                 "", "", "", "", row["Co-Listing Agent First Name"],
                                 row["Co-Listing Agent Last Name"],
                                 "", None, row["Co-Listing Office Name"],
                                 row["Co-Listing Office Phone Number"])

    selling_agent = add_agent(row["Selling Agent Public ID"],
                               row["Selling Office Code"], "",
                               "", "", "", row["Selling Agent First Name"],
                               row["Selling Agent Last Name"],
                               row["SA Dre License Number"],
                               None, row["Selling Office Name"], "")

    co_selling_agent = add_agent(row["Co-Selling Agent Public ID"],
                                 row["Co-Selling Office Code"],
                                 "", "", "", "", row["Co-Selling Agent First Name"],
                                 row["Co-Selling Agent Last Name"],
                                 row["Co SA  BRE License Number"],
                                 None, row["Co-Selling Office Name"], "")

    add_transaction(row["Multiple Listing Number"], row["Status"],
                    row["Date Status Changed"], address, listing_agent,
                    co_listing_agent, selling_agent, co_selling_agent,
                    row["Selling Price"], row["SaleType"], row["Financing"],
                    row["Sold Terms"], row["Assessor's Parcel Number"],
                    row["Structure Attached?"], row["Area"], row["Agent Remarks"],
                    row["Lot Square Footage"], row["Price Per Square Foot"],
                    row["Property Description"], row["Year Built"],
                    row["Type"], row["Cumulative Days on Market"],
                    row["Current Price"], row["Date Closed Sale"],
                    row["Days on Market"])









def save_file(file):
    """
    Accepts a Python file-like object as arguement
    validates file ends in CSV, then saves it.
    returns import id on success, False on fail
    """

    import_object = CsvImport(original_filename = file.name)

    #make sure file ends in .csv
    if '.csv' not in file.name[-4:].lower():
        import_object.status = "Fail"
        import_object.progress = 100
        import_object.save()
        error_description = "The file %s is not a csv file." % file.name
        error = CsvImportLog(csv_import = import_object,
                             type="Error",
                             description=error_description, error="File Not CSV")
        error.save()
        return False

    #save file to disk
    import_object.save()
    saved_file = open(import_object.relative_filepath, "w")
    saved_file.write(file.read())
    saved_file.close()

    import_object.user_message = 'File queued for import'
    import_object.progress = progress_background_queued
    import_object.save()

    return import_object.id




def process_file(import_id, aggregate_on_complete=True):
    import_object = CsvImport.objects.get(pk=import_id)
    import_file = open(import_object.relative_filepath, "r")
    try:
        data = csv.DictReader(import_file)
        issublist(REQUIRED_FIELD, data.fieldnames) # gives error when called later on some files, see if error occurs.
    except Exception as e:
        import_object.status = "Fail"
        import_object.message = "An Error Occured - There was a problem opening the file"
        import_object.progress = 100
        import_object.save()
        error = CsvImportLog(csv_import = import_object,
                             type="Error",
                             description="Error Reading File",
                             dump=str(e),
                             error="File Missing Headers")
        error.save()
        print e
        return False


    # make sure all the required fields are in the csvfile
    if not issublist(REQUIRED_FIELD, data.fieldnames):
        import_object.status = "Fail"
        import_object.message = "An Error Occured - The file you attempted to import is missing required fields"
        import_object.progress = 100
        import_object.save()
        error_description = "Missing headers: %s" % ( ", ".join(items_missing(REQUIRED_FIELD, data.fieldnames)) )
        error = CsvImportLog(csv_import = import_object,
                             type="Error",
                             description=error_description[:100],
                             dump=error_description,
                             error="File Missing Headers")
        error.save()
        return False

    import_object.records_in_import = get_row_count_in_file(import_object.relative_filepath)
    for current_row, reader in enumerate(data):
        #print reader["Multiple Listing Number"]
        try:
            import_row(reader)

        except Exception as e:
            import_object.status = "Warning"
            error_description = "Error Importing Row# %s (MLS# %s)" % (current_row, reader['Multiple Listing Number'] )
            error = CsvImportLog(csv_import = import_object,
                                 type="Error",
                                 description= error_description[:100],
                                 dump=traceback.format_exc(),
                                 error="Row Import Error")
            error.save()
            print e


        if current_row % 5 == 0:
            import_object.progress = calculate_progress(progress_row_processing_start,
                                                        progress_row_processing_end,
                                                        current_row,
                                                        import_object.records_in_import,
                                                        100 if aggregate_on_complete else progress_row_processing_end )
            import_object.user_message = "Processing File - Importing Row %s of %s" % (current_row, import_object.records_in_import)
            import_object.save()

    if aggregate_on_complete:
        print "starting agent aggregation"
        aggregate_agents(import_object)
        print "starting office aggregation"
        aggregate_offices(import_object)


    import_object.progress = 100
    import_object.user_message = "Done"
    if import_object.status is not "Warning":
        import_object.status = "Complete"

    import_object.save()
    return True



def aggregate_agents(import_object=False):
    """
    This function goes through each agent, aggregates transaction count and total transaction volume.
    We need to aggregate the transactions this way since the queries are too intensive on the database.
    """

    all_agents = Agent.objects.all()
    agent_count = Agent.objects.all().count()
    for current_row, agent in enumerate(all_agents):
        agent.total_selling_transaction_volume = int_or_zero(Transaction.objects.filter(selling_agent=agent).aggregate(Sum('selling_price'))['selling_price__sum'])
        agent.total_selling_transaction_count = Transaction.objects.filter(selling_agent=agent).aggregate(Count('selling_price'))['selling_price__count']
        agent.total_listing_transaction_volume = int_or_zero(Transaction.objects.filter(listing_agent=agent).aggregate(Sum('selling_price'))['selling_price__sum'])
        agent.total_listing_transaction_count = Transaction.objects.filter(listing_agent=agent).aggregate(Count('selling_price'))['selling_price__count']
        agent.save()

        if current_row % 5 == 0:

            if import_object:
                import_object.progress = calculate_progress(progress_agent_aggregation_start,
                                                            progress_agent_aggregation_end,
                                                            current_row,
                                                            agent_count)
                import_object.user_message = "Aggregating agent transactions - %s of %s" % (current_row, agent_count)
                import_object.save()



def aggregate_offices(import_object=False):
    all_offices = Office.objects.all()
    office_count = Office.objects.all().count()
    for current_row, office in enumerate(all_offices):
        office.total_selling_transaction_volume = int_or_zero(Agent.objects.filter(office_id=office).aggregate(Sum('total_selling_transaction_volume'))['total_selling_transaction_volume__sum'])
        office.total_selling_transaction_count = Agent.objects.filter(office_id=office).aggregate(Count('total_selling_transaction_count'))['total_selling_transaction_count__count']
        office.total_listing_transaction_volume = int_or_zero(Agent.objects.filter(office_id=office).aggregate(Sum('total_listing_transaction_volume'))['total_listing_transaction_volume__sum'])
        office.total_listing_transaction_count = Agent.objects.filter(office_id=office).aggregate(Count('total_listing_transaction_count'))['total_listing_transaction_count__count']
        office.save()

        if current_row % 5 == 0:

            if import_object:
                import_object.progress = calculate_progress(progress_office_aggregation_start,
                                                            progress_office_aggregation_end,
                                                            current_row,
                                                            office_count)
                import_object.user_message = "Aggregating office transactions - %s of %s" % (current_row, office_count)
                import_object.save()

