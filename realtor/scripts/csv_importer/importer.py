import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../','..'))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "realtor_data.settings")

import file_handler

if __name__ == "__main__":
    try:
        a = sys.argv[1]
    except:
        print "Enter filename of import file as argument -- ex: python importer.py FILE.CSV"
        sys.exit(0)
    import_id = file_handler.save_file( open(sys.argv[1],"r") )

    if import_id:
        print "Starting file processor"
        process_result = file_handler.process_file(import_id, aggregate_on_complete=True)
        if process_result:
            print "Import Completed Successfully"
