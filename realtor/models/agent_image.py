import os
from django.db import models
from agent import Agent
from django.conf import settings
from django.template import Template, Context


def path_and_rename(path):
    def wrapper(instance, filename):
        ext = filename.split('.')[-1]
        if instance.agent.id:
            filename = '{}_{}_{}.{}'.format(instance.agent.id,
                                            instance.agent.last_name,
                                            instance.agent.first_name,
                                            ext.lower())
        else:
            filename = '{}_{}.{}'.format(instance.agent.last_name,
                                            instance.agent.first_name,
                                            ext.lower())
        return os.path.join(path, filename)
    return wrapper


class AgentImage(models.Model):
    agent = models.OneToOneField(Agent, primary_key=True)
    filename = models.CharField(max_length=100)
    image = models.ImageField(upload_to=path_and_rename('agents'), blank=True, null=True)
    has_uploaded = models.BooleanField(default=False)
    width = models.IntegerField(max_length=5, blank=True, null=True)
    height = models.IntegerField(max_length=5, blank=True, null=True)
    crop_x = models.IntegerField(max_length=5, blank=True, null=True)
    crop_y = models.IntegerField(max_length=5, blank=True, null=True)
    crop_width = models.IntegerField(max_length=5, blank=True, null=True)
    crop_height = models.IntegerField(max_length=5, blank=True, null=True)
    micro_box_width = 30
    small_box_width = 75
    medium_box_width = 200


    html = """
            <div class="agent_photo_{{ size }}"
               {% if has_uploaded %}
               style="background-image:url({{ MEDIA_URL }}/{{image}});
               {% else %}
               style="background-image:url({{ STATIC_URL }}image_resizer/{{filename}});
               {% endif %}
               background-size:{{ background_image_width }}px {{ background_image_height }}px;
               background-position:left -{{ background_image_xpos }}px top -{{ background_image_ypos }}px;
            ">
            </div>"""



    @classmethod
    def _get_scale_factor(cls, input_image_size, output_image_size):
        return output_image_size*1.000/input_image_size

    def _to_html(self, output_image_size, div_class):
        t = Template(self.html)
        scale_factor = self._get_scale_factor(self.crop_width, output_image_size)
        context={'STATIC_URL':settings.STATIC_URL,
               'MEDIA_URL':settings.MEDIA_URL,
               'filename':self.filename,
               'image':self.image,
               'has_uploaded':self.has_uploaded,
               'size':div_class,
               'background_image_width':self.width * scale_factor,
               'background_image_height':self.height * scale_factor,
               'background_image_xpos':self.crop_x * scale_factor,
               'background_image_ypos': self.crop_y * scale_factor,
               }
        return t.render(Context(context))


    def to_html_medium(self):
        return self._to_html(self.medium_box_width,"medium")

    def to_html_small(self):
        return self._to_html(self.small_box_width,"small")

    def to_html_micro(self):
        return self._to_html(self.micro_box_width,"micro")

    @classmethod
    def _to_html_unknown_image(cls, output_image_size, css_class):
        unknown_image_width = 419
        unknown_image_height = 498
        t = Template(cls.html)
        scale_factor = cls._get_scale_factor(unknown_image_width, output_image_size)
        context={'STATIC_URL':settings.STATIC_URL,
               'filename':"unknown.jpg",
               'size':css_class,
               'background_image_width':unknown_image_width * scale_factor,
               'background_image_height':unknown_image_height * scale_factor,
               'background_image_xpos':0,
               'background_image_ypos': 20 * scale_factor,
               }
        return t.render(Context(context))

    @classmethod
    def to_html_unknown_image_micro(cls):
        return cls._to_html_unknown_image(cls.micro_box_width, 'micro')

    @classmethod
    def to_html_unknown_image_small(cls):
        return cls._to_html_unknown_image(cls.small_box_width, 'small')

    @classmethod
    def to_html_unknown_image_medium(cls):
        return cls._to_html_unknown_image(cls.medium_box_width, 'medium')



    class Meta:
        verbose_name_plural = "agent_images"
        app_label = "realtor"
        db_table = 'agent_image'
