from django.db import models

from office import Office


class Agent(models.Model):
    id = models.CharField(primary_key=True, max_length=15)
    first_name = models.CharField(db_index=True, blank=True, max_length=50)
    last_name = models.CharField(db_index=True, blank=True, max_length=50)
    office = models.ForeignKey(Office)
    license_number = models.IntegerField(db_index=True, blank=True, null=True, max_length=8)
    email = models.EmailField(max_length=254)
    cell_phone_number = models.CharField(max_length=12)
    direct_phone_number = models.CharField(max_length=12)
    website = models.URLField()
    valid = models.BooleanField(default=True)
    total_selling_transaction_volume = models.IntegerField(null=True, blank=True, default=None)
    total_selling_transaction_count = models.IntegerField(null=True, blank=True,  default=None)
    total_listing_transaction_volume = models.IntegerField(null=True, blank=True, default=None)
    total_listing_transaction_count = models.IntegerField(null=True, blank=True,  default=None)

    @property
    def name(self):
        return "%s %s" % (self.first_name, self.last_name)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = "agents"
        app_label = "realtor"
        db_table = 'agent'
