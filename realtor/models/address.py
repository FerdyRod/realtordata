from django.db import models


class Address(models.Model):
    street_direction = models.CharField(blank=True, max_length=25)
    street_direction_suffix = models.CharField(blank=True, max_length=25)
    street_name = models.CharField(blank=True, max_length=50)
    street_number = models.IntegerField(blank=True, null=True, max_length=10)
    street_number_modifier = models.CharField(blank=True, max_length=25)
    street_suffix = models.CharField(blank=True, max_length=25)
    street_suffix_modifier = models.CharField(blank=True, max_length=25)
    unit_number = models.CharField(blank=True, max_length=10)
    city = models.CharField(db_index=True, max_length=75)
    zip_code = models.IntegerField(blank=True, db_index=True, max_length=5)
    latitude = models.DecimalField(blank=True, max_digits=9, decimal_places=6)
    longitude = models.DecimalField(blank=True, max_digits=9, decimal_places=6)

    @property
    def full_address(self):
        address_string = ""
        if self.street_number:
            address_string = address_string + str(self.street_number)
        if self.street_number_modifier:
            address_string = address_string + " " + str(self.street_number_modifier)
        if self.street_direction:
            address_string = address_string + " " + str(self.street_direction)
        if self.street_direction_suffix:
            address_string = address_string + " " + str(self.street_direction_suffix)
        if self.street_name:
            address_string = address_string + " " + str(self.street_name)
        if self.street_suffix:
            address_string = address_string + " " + str(self.street_suffix)
        if self.street_suffix_modifier:
            address_string = address_string + " " + str(self.street_suffix_modifier)
        if self.unit_number:
            address_string = address_string + " Unit " + str(self.unit_number)
        address_string = address_string + ","
        if self.city:
            address_string = address_string + " " + str(self.city)
        address_string = address_string + ", Ca"
        if self.zip_code:
            address_string = address_string + " " + str(self.zip_code)

        return address_string


    class Meta:
        verbose_name_plural = "addresses"
        app_label = "realtor"
        db_table = 'address'
