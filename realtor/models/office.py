from django.db import models

from address import Address


class Office(models.Model):
    id = models.CharField(primary_key=True, max_length=15)
    address = models.ForeignKey(Address, blank=True, null=True)
    name = models.CharField(db_index=True, blank=True, max_length=50)
    phone = models.CharField(blank=True, max_length=25)
    license_number = models.IntegerField(blank=True, null=True, max_length=15)
    total_selling_transaction_volume = models.IntegerField(null=True, blank=True, default=None)
    total_selling_transaction_count = models.IntegerField(null=True, blank=True,  default=None)
    total_listing_transaction_volume = models.IntegerField(null=True, blank=True, default=None)
    total_listing_transaction_count = models.IntegerField(null=True, blank=True,  default=None)

    class Meta:
        verbose_name_plural = "offices"
        app_label = "realtor"
        db_table = 'office'
