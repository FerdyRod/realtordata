from django.db import models

from address import Address
from agent import Agent


class Transaction(models.Model):


    multiple_listing_number = models.CharField(primary_key=True, max_length=10)
    status = models.CharField(db_index=True, max_length=50)
    date_status_changed = models.DateField(db_index=True, null=True)
    address = models.ForeignKey(Address)
    listing_agent = models.ForeignKey(Agent, related_name="listing_agent_transactions")
    co_listing_agent = models.ForeignKey(Agent, related_name="co_listing_agent_transactions", blank=True, null=True)
    selling_agent = models.ForeignKey(Agent, related_name="selling_agent_transactions", blank=True, null=True)
    co_selling_agent = models.ForeignKey(Agent, related_name="co_selling_agent_transactions", blank=True, null=True)
    selling_price = models.IntegerField(blank=True, max_length=8)
    sale_type = models.CharField(blank=True, max_length=150)
    financing = models.CharField(blank=True, max_length=150)
    sold_terms = models.CharField(blank=True, max_length=150)
    assessors_parcel_number = models.BigIntegerField(blank=True, null=True, max_length=10)
    structure_attached = models.NullBooleanField(blank=True, null=True)
    area = models.CharField(db_index=True, blank=True, max_length=150)
    agent_remarks = models.TextField(blank=True)
    lot_square_footage = models.IntegerField(blank=True, null=True, max_length=10)
    price_per_square_foot = models.DecimalField(blank=True, null=True, max_digits=6, decimal_places=2)
    property_description = models.TextField(blank=True)
    year_built = models.IntegerField(blank=True, max_length=4)
    property_type = models.CharField(blank=True, max_length=150)
    cumulative_days_on_market = models.IntegerField(blank=True, max_length=5)
    current_price = models.IntegerField(blank=True, max_length=8)
    date_closed_sale = models.DateField(blank=True, null=True)
    days_on_market = models.IntegerField(blank=True, max_length=5)

    class Meta:
        verbose_name_plural = "transactions"
        app_label = "realtor"
        db_table = 'transaction'
        ordering = ['-date_closed_sale']
