from django.db import models
from csv_import import CsvImport

class CsvImportLog(models.Model):

    csv_import = models.ForeignKey(CsvImport)
    type = models.CharField(blank=True, db_index=True, max_length=10)
    description = models.CharField(blank=True, max_length=100)
    error = models.CharField(blank=True, max_length=255)
    dump = models.TextField(blank=True)



    class Meta:
        verbose_name_plural = "csv_import_logs"
        app_label = "realtor"
        db_table = 'csv_import_log'
