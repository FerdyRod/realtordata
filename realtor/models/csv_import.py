from django.db import models


class CsvImport(models.Model):

    user_message = models.CharField(blank=True, max_length=100, default="")
    status = models.CharField(blank=True, max_length=15, default="Processing")
    records_in_import = models.IntegerField(blank=True, default=0)
    progress = models.IntegerField(blank=True, default=0)
    original_filename = models.CharField(blank=True, max_length=100)

    file_dir = 'saved_files'

    @property
    def filename(self):
        return "%s.csv" % self.id

    @property
    def relative_filepath(self):
        return "%s/%s" % (self.file_dir, self.filename)

    def __unicode__(self):
        return self.filename


    class Meta:
        verbose_name_plural = "csv_imports"
        app_label = "realtor"
        db_table = 'csv_import'
