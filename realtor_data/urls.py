from django.conf.urls import patterns, include, url
from django.conf import settings
import realtor.views

# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'realtor_data.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # url(r'^admin/', include(admin.site.urls)),

    url(r'^$', realtor.views.home, name='home'),
    url(r'^realtors/$', realtor.views.realtor_list, name='realtor_list'),
    url(r'^realtor/(.+)$', realtor.views.one_realtor, name='one_realtor'),
    url(r'^invalid_realtors/$', realtor.views.invalid_realtor_list, name='invalid_realtor_list'),
    url(r'^image/(.+)$', realtor.views.edit_image, name='edit_image'),
    url(r'^upload/(.+)$', realtor.views.upload_image, name='upload_image'),
    url(r'^office/(.+)$', realtor.views.one_office, name='one_office'),
    url(r'^offices/$', realtor.views.office_list, name='office_list'),
    url(r'^transaction/(.+)$', realtor.views.one_transaction, name='one_transaction'),
    url(r'^admin/$', realtor.views.admin, name='admin'),
    url(r'^imports/$', realtor.views.csv_import_list, name='csv_import_list'),
    url(r'^import/(.+)$', realtor.views.one_csv_import, name='one_csv_import'),
    url(r'^import_json/(.+)$', realtor.views.one_csv_import_json, name='one_csv_import_json'),
    url(r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'realtor/login.html'}, 'login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page':'/login'}, 'logout'),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
)
